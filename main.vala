// valax --pkg json-glib-1.0 main.vala error.vala && VAPD_LISTEN="'http://floofy'" ./main

void main (string[] args) {
	unowned string? listen_specs_str = Environment.get_variable("VAPD_LISTEN");
	if (listen_specs_str == null) 
		vapd_error(0, "missing required parameter 'VAPD_LISTEN'");
	
	Json.Node? listen_specs_node = null;
	try {
		listen_specs_node = Json.from_string("[" + (!) listen_specs_str + "]");
	} catch (Error err) {
		vapd_error(1, "invalid format for 'VAPD_LISTEN'");
	}
	if (listen_specs_node == null)
		vapd_error(2, "'VAPD_LISTEN' must be non-null");
	
	Json.Array? listen_specs_json_arr = ((!) listen_specs_node).get_array();
	if (listen_specs_json_arr == null)
		vapd_error(3, "error parsing 'VAPD_LISTEN'");
		
	bool not_listening = true;
	
	for (uint i = 0, l = ((!) listen_specs_json_arr).get_length(); i < l; i++) {
		string listen_spec = ((!) listen_specs_json_arr).get_string_element(i);
		not_listening = false;
		// valid listen specs
		//   http://hostname:port
		//   http://ipv4:port
		//   http://ipv6:port
		//   http:///unix/path      : for unix sockets
		//   pipe:http:///file/path : for unix named pipes
		//   pipe:http:///file/path1;/file/path2 : for unix pipes, with separate read and write sides. use /dev/fd/ for fds (stdin, stdout)
		if (listen_spec.str("http://") == listen_spec) {
			print("http at %s\n", listen_spec);
		} else if (listen_spec.str("pipe:http://") == listen_spec) {
			print("pipe at %s\n", listen_spec);
		} else {
			vapd_error(5, "unrecogized listen specifier '%s'".printf(listen_spec));
		}
	}
	
	if (not_listening) {
		vapd_error(4, "must listen on at least one port");
	}
	
	print("Hewwo World :3\n");
}

